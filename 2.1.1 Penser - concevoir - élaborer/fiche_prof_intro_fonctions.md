**Thématique :** Algorithmique : fonctions

**Notions liées :** Python

**Résumé de l’activité :** Activité sur poste pour introduire les fonctions (procédures) avec turtle

**Objectifs :** Faire découvrir les fonctions et comment elles permettent de factoriser des instructions

**Auteur :** Mireille Coilhac

**Durée de l’activité :** 2h 

**Prérequis :** : Les bases de Python : affectations, if, for, while ...

**Forme de participation :** individuelle ou en binôme, en autonomie.

**Matériel nécessaire :** ordinateur ou tablette avec Python installé ou en ligne

**Préparation :** Aucune

**Autres références :** 

**Fiche élève cours :** 

**Fiche élève activité :** [sujet en pdf](https://gitlab.com/mcoilhac/mooc-2-ressources/-/blob/main/2.1.1%20Penser%20-%20concevoir%20-%20%C3%A9laborer/fonctions_turtle_2022.pdf)
