# Partrie 1
##  Sous-partie 1 : texte
Une phrase sans rien  
*Une phrase en italique*  
**Une phrase en gras**  
[Un lien](https://www.fun-mooc.fr/fr/)  
une ligne `code local`
## Sous-partie 2 : listes
**Liste à puce**  
* item  
   * sous item 3 espaces en début de ligne
   * sous item
* item

## Le code
```python
# code python
```
## Une citation
> Deb  
> Suite  
> Fin   

Fin de citation
